﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ProjectileInfo
{
    public ProjectileType type;
    public bool collision;
    public Vector3 startPosition,direction;
    public float speed, gravity;
    public BaseHero sender;
}

public class Projectile : MonoBehaviour
{
    private const float SPEED_TO_METER = 60.0f;

    public GameObject thisGameObject;
    public Transform thisTransform;
    public Rigidbody rigid;
    public Collider collider;

    public ProjectileType type;
    public BaseHero sender;
    public float speed;
    public bool isActive;
    public Vector3 direction;

    // Action
    public Action onHit;
    public void PassInfo(Action action)
    {
        this.onHit = action;
    }

    private void Awake()
    {
        thisTransform = transform;
        thisGameObject = gameObject;
        rigid = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
    }

    public void Launch(ProjectileInfo info)
    {
        type = info.type;
        sender = info.sender;
        direction = info.direction;
        speed = info.speed;
        thisTransform.position = info.startPosition;
        isActive = true;
        Physics.IgnoreCollision(info.sender.GetComponent<Collider>(), GetComponent<Collider>());
    }

    public void UpdateProjectile()
    {
        Vector3 moveDelta = direction * speed;
        rigid.velocity = moveDelta;
    }

    private void OnTriggerEnter(Collider col)
    {
        onHit.Invoke();
        onHit = null;
    }
}
