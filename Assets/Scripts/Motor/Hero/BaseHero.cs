﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Abilities
{
    PrimaryFire,
    SecondaryFire,
    Ability1,
    Ability2,
    Ability3,
}

public class BaseHero : MonoBehaviour
{
    protected BaseMotor motor;
    public List<BaseAbility> abilities = new List<BaseAbility>();

    private void Awake()
    {
        motor = GetComponent<BaseMotor>();
    }

    protected virtual void Update()
    {
        PoolInput();
    }

    private void PoolInput()
    {
        if (InputManager.IsDown(InputName.PrimaryFire)) // Left Click
        {
            CastAbility(Abilities.PrimaryFire);
        }
        else if (InputManager.IsDown(InputName.SecondaryFire)) // Right Click
        {
            CastAbility(Abilities.SecondaryFire);
        }

        if (InputManager.IsDown(InputName.Ability1))
        {
            CastAbility(Abilities.Ability1);
        }
        if (InputManager.IsDown(InputName.Ability2))
        {
            CastAbility(Abilities.Ability2);
        }
        if (InputManager.IsDown(InputName.Ability3))
        {
            CastAbility(Abilities.Ability3);
        }
    }

    protected virtual void CastAbility(Abilities ab)
    {
        Debug.Log("Cast Ability not implemented in : " + this.name);
    }
}
